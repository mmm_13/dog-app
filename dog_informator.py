"""
Manager informacji o rasach psów
"""

import requests


class Dog:

    def __init__(self, features_list):
        self.info_list = features_list


class Manager:

    def __init__(self):
        self.key = ''
        self.dogs = {}
        self.checking_list = []

    def define_key(self, key):
        self.key = key

    def import_dog_info(self, name):
        name = name.lower()
        url = "https://wikiapi.p.rapidapi.com/api/v1/wiki/animals/dog/info/{}".format(name)
        print(url)

        querystring = {"lan": "en"}

        headers = {
            'x-rapidapi-host': "wikiapi.p.rapidapi.com",
            'x-rapidapi-key': self.key
        }

        response = requests.request("GET", url, headers=headers, params=querystring)

        if response.status_code != 200:
            return "Brak"

        return response.json()

    def update_data(self, name):

        data_dict = self.import_dog_info(name)

        if data_dict == 'Brak':
            return data_dict

        dogs_name = data_dict.get("name")
        link = data_dict.get("photo_img")
        title = data_dict.get("photo_desc")
        origin = data_dict.get("origin")
        color = data_dict.get("color")
        coat = data_dict.get("coat")
        life = data_dict.get("life_span")
        more = data_dict.get("fci")
        features_list = [dogs_name, link, title, origin, color, coat, life, more]
        self.dogs[name] = Dog(features_list)
        return "OK"

    def check_dog_info(self, name):
        if name in self.dogs:
            return self.dogs[name]

        result = self.update_data(name)
        if result == "OK":
            return self.dogs[name]
        return result


informator = Manager()
