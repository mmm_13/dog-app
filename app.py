from flask import Flask, request, render_template
import torch
from PIL import Image
from torchvision import transforms
import os
from dogs_recognition_model import dogs_model
import numpy as np
from dog_informator import informator

app = Flask(__name__, static_folder='/static')
UPLOAD_FOLDER = os.path.join('/static', 'uploads')

if not os.path.exists(UPLOAD_FOLDER):
    os.mkdir('/static')
    os.mkdir(UPLOAD_FOLDER)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
recognizer = dogs_model
recognizer.to(device)


def transform_image_to_vector(image):
    my_transform = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize([0.4764, 0.4523, 0.3915],
                             [0.2327, 0.2280, 0.2276])])

    return my_transform(image).unsqueeze(0).contiguous()  # sztuczny batch


classes = sorted(os.listdir('model_train\\dataset\\train'))


def get_prediction(image, model):
    tensor = transform_image_to_vector(image).to(device)
    with torch.no_grad():
        prediction = model(tensor).to('cpu')
        predicted_class = np.argmax(prediction)
    return classes[predicted_class]


# start
@app.route('/')
def start_app():
    return render_template("main.html")


@app.route('/img_resp/', methods=['POST'])
def resp_page():
    if request.method == 'POST':
        if 'img' not in request.files:
            return 'No file in form'
        image = request.files['img']
        path = os.path.join(app.config['UPLOAD_FOLDER'], image.filename)
        image.save(path)
        img = Image.open(path)
        dog_name = get_prediction(img, recognizer)
        if dog_name not in informator.checking_list:
            informator.checking_list.append(dog_name)

    return render_template("img_resp.html", result=dog_name, user_image=path)


@app.route('/main/')
def main_page():
    return render_template('main.html', history=informator.checking_list)


@app.route('/info/')
def info_page():
    if informator.key == '':
        return render_template('api_log.html')
    dog = informator.check_dog_info(informator.checking_list[-1])
    if type(dog) == str:
        return render_template('no_info.html', info=dog)
    features = dog.info_list
    return render_template('info.html', features=features)


@app.route('/key/', methods=['POST'])
def login():
    resp_dict = dict(request.form)
    key = resp_dict['key']
    informator.define_key(key)
    dog = informator.check_dog_info(informator.checking_list[-1])
    if type(dog) == str:
        return render_template('no_info.html', info=dog)
    features = dog.info_list
    return render_template('info.html', features=features)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
