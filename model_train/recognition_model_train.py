import os
from torchvision import datasets
from torchvision.transforms import Compose, ToTensor, Normalize, Resize, RandomRotation, RandomHorizontalFlip
import torchvision.models as models
from torch.utils.data import DataLoader
import torch.nn as nn
from torch.nn import CrossEntropyLoss
import pytorch_lightning as pl
import torchmetrics
import torch

#  Using GPU
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('DEVICE =', device)


def labels_list_create(path):
  labels = []
  for x in sorted(list(os.listdir(path))):
    for y in list(os.listdir(os.path.join(path,x))):
      labels.append(x)
  return labels


def organize_datasets(root_dir):

  train_path = os.path.join(root_dir, "train")
  val_path = os.path.join(root_dir, "validation")
  test_path = os.path.join(root_dir, "test")

  size = [224, 224]  # standard size for images

  #  temporary dataset to normalize data
  transform = Compose([Resize(size), ToTensor()])
  dset = datasets.ImageFolder(train_path, transform)
  loader = DataLoader(dset, batch_size=64, shuffle=False)

  nimages = 0
  mean = 0.0
  var = 0.0
  for i_batch, batch_target in enumerate(loader):
    batch = batch_target[0]
    batch = batch.view(batch.size(0), batch.size(1), -1)
    nimages += batch.size(0)
    mean += batch.mean(2).sum(0)
    var += batch.var(2).sum(0)

  mean /= nimages
  var /= nimages
  std = torch.sqrt(var)

  normalize = Normalize(mean=mean, std=std)

  train_transform = Compose([Resize(size), RandomRotation(45), RandomHorizontalFlip(), ToTensor(), normalize])
  val_transform = Compose([Resize(size), ToTensor(), normalize])

  train_ds = datasets.ImageFolder(train_path, train_transform)
  train_labels = labels_list_create(train_path)
  val_ds = datasets.ImageFolder(val_path, val_transform)
  val_labels = labels_list_create(val_path)
  test_ds = datasets.ImageFolder(test_path, val_transform)
  test_labels = labels_list_create(test_path)

  return ({'train': train_ds, 'val': val_ds, 'test': test_ds},
          {'train': train_labels, 'val': val_labels, 'test': test_labels})


class RecognitionNet(nn.Module):

  def __init__(self, class_amount):
    super().__init__()

    self.class_amount = class_amount

    core = models.resnet50(pretrained=True)
    num_size = core.fc.in_features

    layers = list(core.children())[:-1]
    self.features_extractor = nn.Sequential(*layers)

    self.fc = nn.Sequential(nn.Linear(num_size, self.class_amount))

  def forward(self, x):
    self.features_extractor.eval()
    with torch.no_grad():
      x = self.features_extractor(x)
      x = x.flatten(1)
    x = self.fc(x)
    return x


class RecognitionModel(pl.LightningModule):

  def __init__(self, data_root, batch_size_train, batch_size_test, class_amount):

    super().__init__()
    self.save_hyperparameters()
    self.data_root = data_root
    self.batch_size_train = batch_size_train
    self.batch_size_test = batch_size_test
    self.class_amount = class_amount
    self.net = RecognitionNet(self.class_amount)
    self.loss = CrossEntropyLoss()

  def forward(self, x):
    return self.net(x)

  def training_step(self, batch, batch_idx):
    x, labels = batch
    outputs = self.forward(x)
    loss = self.loss(outputs, labels)
    self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)

    return loss

  def validation_step(self, batch, batch_idx):
    x, labels = batch
    outputs = self.forward(x)
    loss = self.loss(outputs, labels)
    preds = torch.argmax(outputs, dim=1)
    acc = torchmetrics.functional.accuracy(preds, labels)
    self.log('val_loss', loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)
    self.log("val_acc", acc, on_step=True, on_epoch=True, prog_bar=True, logger=True)

    return loss, acc

  def test_step(self, batch, batch_idx):
    x, labels = batch
    outputs = self.forward(x)
    loss = self.loss(outputs, labels)
    preds = torch.argmax(outputs, dim=1)
    acc = torchmetrics.functional.accuracy(preds, labels)
    self.log('test_loss', loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)
    self.log("test_acc", acc, on_step=True, on_epoch=True, prog_bar=True, logger=True)

    return loss, acc

  def prepeare_data(self):
    dataset_dict, labels_list = organize_datasets(root_dir=self.data_root)
    self.ds_train = dataset_dict['train']
    self.ds_val = dataset_dict['val']
    self.ds_test = dataset_dict['test']
    self.labels_train = labels_list['train']
    self.labels_val = labels_list['val']
    self.labels_test = labels_list['test']

  def train_dataloader(self):
    return DataLoader(self.ds_train, batch_size=self.batch_size_train, shuffle=True, pin_memory=True)

  def val_dataloader(self):
    return DataLoader(self.ds_val, batch_size=self.batch_size_train, shuffle=False, pin_memory=True)

  def test_dataloader(self):
    return DataLoader(self.ds_test, batch_size=self.batch_size_test, shuffle=False, pin_memory=True)

  def configure_optimizers(self):
    optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
    return optimizer


class_names = sorted(os.listdir('model_train\\dataset\\train'))

class_limit = len(class_names)

#  Zdefiniowanie loggera

pl_PATH = 'logs'

if not os.path.exists(pl_PATH):
  os.mkdir(pl_PATH)

logger = pl.loggers.TensorBoardLogger(pl_PATH, name='v1')

trainer = pl.Trainer(max_epochs=100, gpus=1, logger=logger, progress_bar_refresh_rate=20)

model = RecognitionModel('model_train\\dataset', batch_size_train=64, batch_size_test=16, class_amount=class_limit)

model.prepeare_data()

trainer.fit(model)

#  Zapisanie wag
torch.save(model.state_dict(), 'model_train\\model_weights.pth')

#  Test
trainer.test(model)
