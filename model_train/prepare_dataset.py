import os
import shutil

cwd = os.getcwd()
data_path = os.path.join(cwd, 'images.tar')  # zbiór danych treningowych z Kaggle

if os.path.exists(os.path.join(cwd, 'dataset')):
    exit('Dane są już przygotowane do wykorzystania')

dir = os.path.join(cwd, 'data')
os.mkdir(dir)

shutil.unpack_archive(data_path, dir, format='tar')

data_dir = os.path.join(dir, 'Images')

classes = sorted(os.listdir(data_dir))
print('Old names:')
print(classes)

class_names = []
for x in classes:
    name = x.split('-')[1].upper()
    class_names.append(name)

print('\nNew names:')
print(class_names)

# Creating dataset dir
all_data = (os.path.join(cwd, 'dataset'))
train_data = (os.path.join(cwd, 'dataset/train'))
validation_data = (os.path.join(cwd, 'dataset/validation'))
test_data = (os.path.join(cwd, 'dataset/test'))
data_paths = [all_data, train_data, validation_data, test_data]

for path in data_paths:
    if not os.path.exists(path):
        os.mkdir(path)

data_paths.pop(0)

image_amount = []

for label in sorted(os.listdir(data_dir)):
    path = os.path.join(data_dir, label)
    x = len(os.listdir(path))
    image_amount.append(x)

print('Maximum amount of images per class = ', max(image_amount))
print('Minimum amount of images per class = ', min(image_amount))


def make_files_list(path):
    lista = []
    with os.scandir(path) as dir:
        for x in dir:
            if not x.is_dir():
                lista.append(x.path)
    return lista


def copy_images(original, data_paths, original_labels, new_labels, image_amount_list):
    for index, (old_label, new_label) in enumerate(zip(original_labels, new_labels)):
        lista = []
        for x in data_paths:
            if not os.path.exists(x):
                os.makedirs(x)
            path = os.path.join(x, new_label)
            lista.append(path)
            if not os.path.exists(path):
                os.makedirs(path)
        i = 0
        j = 0
        k = 0
        amount = image_amount_list[index]
        for image in make_files_list(os.path.join(original, old_label)):
            src = image

            if i in range(0, int(amount * 0.8)):
                cur = '{0:03d}.jpg'.format(i)
                dst = os.path.join(lista[0], cur)
                shutil.copyfile(src, dst)
            elif i in range(int(amount * 0.8), int(amount * 0.9)):
                cur = '{0:03d}.jpg'.format(j)
                dst = os.path.join(lista[1], cur)
                shutil.copyfile(src, dst)
                j += 1
            else:
                cur = '{0:03d}.jpg'.format(k)
                dst = os.path.join(lista[2], cur)
                shutil.copyfile(src, dst)
                k += 1

            i += 1


copy_images(data_dir, data_paths, classes, class_names, image_amount)

shutil.make_archive(all_data, "zip", cwd, 'dataset')

shutil.rmtree(dir)

print('Dane przygotowane do wykorzystania')





